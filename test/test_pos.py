#!/usr/bin/env python
from __future__ import print_function

from blockclique_pos.experiment import Experiment


n_threads = 32
n_nodes = 256
time_between_blocks = 16.  # seconds
max_transactions_per_block = 5200  # transactions
average_transactions_per_second = 5200  # transactions per second
block_verification_time = 0.050  # second per block
transaction_verification_time = 0.000025  # second per transaction
transaction_size = 1024  # bits
block_header_size = 872  # bits
average_bandwidth = 32000000  # bits per second (upload)
average_latency = 0.1  # second
finality = 64
draw_graph = True
real_time = False

xp = Experiment(0,
                n_threads,
                n_nodes,
                time_between_blocks,
                max_transactions_per_block,
                average_transactions_per_second,
                block_verification_time,
                transaction_verification_time,
                transaction_size,
                block_header_size,
                average_bandwidth,
                average_latency,
                finality,
                draw_graph,
                real_time)

xp.run(3600)

xp.compute_stats()

print("Statistics:", xp.stats)
