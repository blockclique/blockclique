# -*- coding: utf-8 -*-
"""
Implementation of blocks of transactions.

"""


class Block(object):
    """Block of transactions."""
    def __init__(self, thread_id, block_id, timestamp,
                 block_references, node_id, n_transactions,
                 generation, reward, target, block_hash):
        """Create a block.

        :param thread:
        :param block_id:
        :param timestamp:
        :param block_references: Reference one block per thread
        :param node_id: Identifier of the miner who discovered this block
        :param n_transactions:
        :param generation:
        :param reward:
        :param target:
        :param block_hash:
        """

        self.thread_id = thread_id
        self.block_id = block_id
        self.timestamp = timestamp
        self.block_references = block_references
        self.node_id = node_id
        self.n_transactions = n_transactions
        self.generation = generation
        self.reward = reward
        self.target = target
        self.block_hash = block_hash
        self.status = None  # Becomes "final" or "stale"
        # We record the time when this block is removed from
        # the incremental blockclique as a final block,
        # to compute statistics about confirmation time.
        self.timestamp_removed_final = None
