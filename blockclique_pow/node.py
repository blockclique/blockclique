# -*- coding: utf-8 -*-
"""
Implementation of mining nodes.

"""
import numpy as np

from blockclique_pow.block import Block
from blockclique_pow.block_graph import BlockGraph


class Node(object):
    """Mining Node."""
    def __init__(self, node_id, n_threads, n_nodes, time_between_blocks,
                 finality, genesis_block_dict, transaction_pool,
                 mining_power=1000., attacker=None):
        """Create a mining node.

        :param node_id: Identifier of the node
        :param n_threads: Number of threads
        :param n_nodes: Number of nodes
        :param time_between_blocks: Average Time between two blocks in a thread
        :param finality: Finality parameter
        :param genesis_block_dict: Initial block dictionary
        :param transaction_pool: Transaction pool
        """
        self.node_id = node_id
        self.n_threads = n_threads
        self.n_nodes = n_nodes
        self.time_between_blocks = time_between_blocks
        self.finality = finality
        self.attacker = attacker
        # Assign node to a thread (deterministic here, but random in general)
        self.thread = self.node_id % self.n_threads
        # Initialize time when a block is found by Node
        self.time_next_block = 0.
        # Initialize block graph
        self.block_graph = BlockGraph(self.node_id, self.n_threads,
                                      self.thread, genesis_block_dict,
                                      self.time_between_blocks, self.finality)
        # Initialize references
        self.block_references = range(self.n_threads)
        # Initialize mining
        if self.attacker and self.thread == 0 and self.node_id == 0:
            p = self.attacker["p"]
            if self.attacker["txids"] == "all":
                self.n_current_transactions = (
                    transaction_pool.get_transactions(
                        0.,
                        self.block_references[self.thread]))
            elif self.attacker["txids"] == "none":
                self.n_current_transactions = 0
            else:
                raise NotImplementedError
            if self.thread == 0 and self.node_id == 0:
                self.mining_power = mining_power * self.n_nodes * p
            else:
                self.mining_power = (mining_power * (1-p)
                                     * self.n_nodes / (self.n_nodes - 1))
        else:
            self.n_current_transactions = transaction_pool.get_transactions(
                0.,
                self.block_references[self.thread]
                )
            self.mining_power = mining_power
        self.next_block_target = self.block_graph.next_block_target()
        self.mine(0.)
        # Total rewards received (used for statistics)
        self.rewards = 0.

    def compute_reward(self, generation):
        """Compute the reward of a block.

        The reward scheme is here a fixed total supply of 20,000,000 tokens
        with half of the remaining tokens created every 10 years
        :param generation: Generation of the block
        """
        r1 = 0.0438741
        q = 0.99999993
        return int(r1 * q ** (generation - 1) * 10. ** 8.) * 10. ** (- 8.)

    def process_block(self, time_reception, block_id, transaction_pool):
        """Process a received block.

        :param time_reception: Time of reception
        :param block_id: Identifier of the block
        :param block_dict: Shared dictionary of blocks
        :param transaction_pool: Transaction pool
        """
        # Update block references
        old_parent = self.block_references[self.thread]
        self.block_references = self.block_graph.update(block_id)
        # Update next block target based on new references
        if old_parent != self.block_references[self.thread]:
            self.next_block_target = self.block_graph.next_block_target()
        # Update number of unconfirmed transactions in thread
        if self.attacker and self.thread == 0 and self.node_id == 0:
            if self.attacker["txids"] == "all":
                self.n_current_transactions = (
                    transaction_pool.get_transactions(
                        time_reception,
                        self.block_references[self.thread]))
            elif self.attacker["txids"] == "none":
                self.n_current_transactions = 0
            else:
                raise NotImplementedError
        else:
            self.n_current_transactions = transaction_pool.get_transactions(
                time_reception,
                self.block_references[self.thread]
                )

    def mine(self, time_start_mining):
        """Compute the time when this node will discover a block.

        The time between two blocks is an exponential law.
        All nodes have the same mining power here.

        :param time_start_mining: Time when this node starts mining
        """
        self.time_next_block = time_start_mining + np.random.exponential(
            1. / (self.next_block_target * self.mining_power))

    def create_block(self, block_id, timestamp, block_dict):
        """Create a block.

        :param block_id: Identifier of the new block
        :param timestamp: Timestamp of the new block
        :param block_dict: Shared dictionary of blocks
        """
        # Compute block generation (incremental)
        generation = (block_dict[self.block_references[self.thread]].generation
                      + 1)
        # Compute block reward based on generation
        reward = self.compute_reward(generation)
        # Put a random hash, we do not simulate them in detail here
        block_hash = self.next_block_target * np.random.random()
        # Compute the time of the next block discovery by this node
        self.mine(timestamp)
        # Return the newly discovered block
        return Block(self.thread, block_id, timestamp, self.block_references,
                     self.node_id, self.n_current_transactions, generation,
                     reward, self.next_block_target, block_hash)
