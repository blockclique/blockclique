# -*- coding: utf-8 -*-
"""
Implementation of Multithreaded Graphs of Parallel Blocks.

"""
import numpy as np
import networkx as nx
import matplotlib.pyplot as plt


class ThinGraph(nx.Graph):
    """ Undirected graph that saves some memory by removing edge info."""
    all_edge_dict = {}

    def single_edge_dict(self):
        """ Share the same dictionary for all edges."""
        return self.all_edge_dict
    edge_attr_dict_factory = single_edge_dict

    def fast_add_edge(self, u, v):
        """ Add edge directly, assume nodes are present."""
        self._adj[u][v] = self.all_edge_dict
        self._adj[v][u] = self.all_edge_dict


class ThinDiGraph(nx.DiGraph):
    """ Directed graph that saves some memory by removing edge info."""
    all_edge_dict = {}

    def single_edge_dict(self):
        """ Share the same dictionary for all edges."""
        return self.all_edge_dict
    edge_attr_dict_factory = single_edge_dict

    def fast_add_edge(self, u, v):
        """ Add edge directly, assume nodes are present."""
        self._succ[u][v] = self.all_edge_dict
        self._pred[v][u] = self.all_edge_dict


class BlockGraph(object):
    """Multithreaded Graph of Parallel Blocks.

    This class represents the discovered blocks and computes the cliques,
    the blockclique, and the references to use to mine the next block.
    """
    def __init__(self, node_id, n_threads, thread_id, genesis_block_dict,
                 time_between_blocks, finality):
        """Create a block graph.

        :param node_id: Identifier of the node creating this block graph
        :param n_threads: Number of threads
        :param genesis_block_dict: Initial block dictionary
        :param finality: Finality parameter
        :param time_between_blocks: Time between two blocks in each thread
        """
        self.node_id = node_id
        self.n_threads = n_threads
        self.thread_id = thread_id
        self.finality = finality
        self.time_between_blocks = time_between_blocks
        # Initialize a list of received blocks not processed yet
        self.received_blocks = []
        # Initialize a dictionary of all blocks (shared within thread)
        self.block_dict = genesis_block_dict
        # Initialize the directed acyclic block graph with genesis blocks
        self.graph = ThinDiGraph()
        for block_id in self.block_dict:
            self.graph.add_node(block_id)
        # Initialize a topological order of graph blocks
        self.tsort_graph = list(range(self.n_threads))
        # Initialize last cross references
        # The last cross reference of thread1 to thread2 is the
        # block of thread 2 referenced by the last block of thread 1
        # removed from the incremental graph as a final block.
        # Those are used when we decide which block can be definitively
        # removed from self.graph because it cannot be referenced
        # by a new block anymore.
        self.cross_refs = dict()
        for thread1 in range(self.n_threads):
            self.cross_refs[thread1] = dict()
            for thread2 in range(self.n_threads):
                self.cross_refs[thread1][thread2] = thread2
        # Initialize the incremental directed acyclic block graph
        self.incremental_graph = ThinDiGraph()
        for block_id in self.block_dict:
            self.incremental_graph.add_node(block_id)
            # Store the descendants of each block of the incremental graph
            self.incremental_graph.nodes[block_id]["descendants"] = set()
        # Initialize a topological order of incremental graph blocks
        self.tsort_incr_graph = list(range(self.n_threads))
        # Initialize a direct incompatibility undirected graph.
        # A direct incompatibility between two blocks is a thread or grandpa
        # incompatibility between those blocks, and is represented by an edge.
        self.direct_incompatibility_graph = ThinGraph()
        self.direct_incompatibility_graph.add_nodes_from(self.graph)
        # Initialize a constraint undirected graph.
        # A constraint between two blocks means that those two blocks cannot
        # appear in the same clique, either because they are directly
        # incompatible, or because one of the ancestors of the first block is
        # incompatible with one of the ancestors of the second block.
        self.constraint_graph = ThinGraph()
        self.constraint_graph.add_nodes_from(self.graph)
        # Initialize a compatibility graph, complement of the constraint graph
        self.compatibility_graph = ThinGraph()
        self.compatibility_graph.add_node(0)
        for i in range(self.n_threads):
            for j in range(i + 1, self.n_threads):
                self.compatibility_graph.add_edge(i, j)
        # Initialize the current references: the last block
        # of each thread in the blockclique.
        self.current_references = range(self.n_threads)
        # Initialize the blockclique: the clique of compatible blocks
        # with the maximum work sum, and if tie, the minimum hash sum.
        self.blockclique = set(range(self.n_threads))
        # Initialize the list of cliques of compatible blocks
        self.cliques = [self.blockclique]
        # Initialize the last block in each thread removed from
        # the incremental graph as final blocks.
        self.last_removed_final = [-1] * self.n_threads
        # Initialize the lasts final blocks removed from self.graph
        self.last_final_blocks = set()
        # Initialize the lasts stale blocks removed from self.incremental_graph
        self.last_stale_blocks = set()
        # The following is only used for statistics
        self.n_cliques = 0
        self.n_final_blocks = 0
        self.n_stale_blocks = 0
        self.n_thread_incompatibilities = 0
        self.n_grandpa_incompatibilities = 0
        self.n_processed_transactions = 0

    def next_block_target(self):
        """Compute the target of the next block based on current references

        We estimate the block rate with the ancestors (in the last 5 minutes)
        of the parent (in same thread) of the next block.
        Reminder: we cannot use the blockclique nor stale blocks.
        """
        # Get the parent (in same thread) of next block
        parent = self.current_references[self.thread_id]
        if self.block_dict[parent].timestamp > 300:
            if self.block_dict[parent].block_references:
                # Compute the maximum timestamp as the timestamp of the
                # last parent of parent.
                max_timestamp = max(
                    self.block_dict[self.block_dict[
                        parent].block_references[thread]].timestamp
                    for thread in range(self.n_threads))
                min_timestamp = max_timestamp - 300.
                # Initialize the number of compatible blocks that appeared
                # in all threads between those timestamps
                n_blocks = 0
                # Initialize the sum of targets
                targets = 0.
                # Follow each parent reference backwards until min timestamp
                for thread in range(self.n_threads):
                    b = self.block_dict[parent].block_references[thread]
                    while (b in self.block_dict
                           and self.block_dict[b].block_references
                           and self.block_dict[b].timestamp > min_timestamp):
                        # Count blocks and targets
                        n_blocks += 1
                        targets += self.block_dict[b].target
                        # b becomes its parent in same thread
                        b = self.block_dict[b].block_references[thread]
                # Compute the average target of all (compatible)
                # parents blocks.
                avg_target = targets / float(n_blocks)
                # Estimate the time between two blocks in each thread
                estimated_time_between_blocks = (self.n_threads
                                                 * (max_timestamp
                                                    - min_timestamp)
                                                 / n_blocks)
                # Estimate a new target
                new_target = (avg_target
                              * estimated_time_between_blocks
                              / self.time_between_blocks)
                # Return a value in between old and new target
                return new_target
        # Otherwise the target do not change
        return self.block_dict[self.current_references[self.thread_id]].target

    def update(self, block_id):
        """Update the blockclique with a new block.

        :param block_id: The id of a block to be processed
        """
        # Add block_id to the list of blocks to be processed
        self.received_blocks.append(block_id)
        # Process received blocks for which all parents
        # have already been processed.
        added_block = True
        while added_block:
            added_block = False
            for i in range(len(self.received_blocks)):
                b_id = self.received_blocks[i]
                # Check if all parents of "b_id" and its grand-parent
                # in its thread are all in self.graph.
                if (all([self.graph.has_node(block_referenced)
                         for block_referenced
                         in self.block_dict[b_id].block_references])
                        and ((not self.block_dict[
                            self.block_dict[b_id].block_references[
                                self.block_dict[
                                    b_id].thread_id]].block_references)
                             or self.block_dict[
                                 self.block_dict[b_id].block_references[
                                     self.block_dict[
                                         b_id].thread_id]].block_references[
                                             self.block_dict[
                                                 b_id].thread_id]
                             in self.graph)):
                    # Process the block "b_id"
                    # Update the graph and incremental graph with the block
                    self.update_graph(b_id)
                    # Compute the thread and grandpa incompatibilities
                    # of the block "b_id" with other blocks.
                    self.compute_incompatibilities(b_id)
                    # Update the blockclique
                    self.update_blockclique(b_id)
                    # Update the references based on the blockclique
                    self.update_references()
                    # Remove blocks from the incremental graph
                    # based on the finality parameter.
                    if self.reduce_incremental_graph(b_id):
                        # If final blocks have been removed from the
                        # incremental graph, we then check if blocks can be
                        # removed from the memory of the node, to save RAM
                        # space in this simulation, and to save disk space
                        # in real. As it takes some time, we only check it
                        # once in a while.
                        if np.random.random() < 0.2:
                            self.reduce_graph()
                    # Once the block is processed, we remove it from the
                    # list of received blocks not processed yet.
                    self.received_blocks.pop(i)
                    added_block = True
                    break
        return self.current_references

    def update_graph(self, block_id):
        """Add the new block to the different graphs.

        :param block_id: The id of the block being processed
        """
        # Add the block being processed to the graph
        self.graph.add_node(block_id)
        # Update the topological order of blocks
        self.tsort_graph.append(block_id)
        self.tsort_incr_graph.append(block_id)
        # Add the block to the incremental graph
        self.incremental_graph.add_node(block_id)
        # Initialize the descendants of this block
        self.incremental_graph.nodes[block_id]["descendants"] = set()
        # Add references of the block in each thread
        for thread in range(self.n_threads):
            # Add the edge (ref, block_id) in the graph
            ref = self.block_dict[block_id].block_references[thread]
            self.graph.fast_add_edge(ref, block_id)
            # Add the edge to the incremental graph if the ref is still there.
            # The incremental graph may not contain a referenced block as
            # blocks are removed when they are considered final.
            if ref in self.tsort_incr_graph:
                self.incremental_graph.fast_add_edge(ref, block_id)

    def has_path_thread(self, b_from, b_to):
        """Check if there is a path between two blocks of the same thread

        :param b_from:
        :param b_to:
        """
        # Start from b_to and move backwards in the same thread until b_from is
        # found, or a genesis block, or go out of the incremental graph.
        b = b_to
        while True:
            if b == b_from:
                return True
            if not self.block_dict[b].block_references:
                return False
            if b not in self.tsort_incr_graph:
                return False
            b = self.block_dict[b].block_references[
                self.block_dict[b].thread_id]

    def compute_incompatibilities(self, block_id):
        """Compute new incompatibilities.

        Compute thread and grandpa incompatibilities between the block
        being processed and any other block.

        :param block_id: The id of the block being processed
        """
        # The direct incompatibility graph represents a thread or
        # grandpa incompatibility between two blocks with an undirected edge.
        self.direct_incompatibility_graph.add_node(block_id)
        #
        # The constraint graph represents direct incompatibilities and
        # indirect incompatibilities: there is a constraint between two blocks
        # if they are directly incompatible or if one of the ancestors of the
        # first block is directly incompatible with one of the ancestors of
        # the second block.
        self.constraint_graph.add_node(block_id)
        # Add all edges from block_id in the compatibility graph
        self.compatibility_graph.add_node(block_id)
        for v in self.tsort_incr_graph:
            if block_id != v:
                self.compatibility_graph.fast_add_edge(block_id, v)
        #
        # First we look for thread incompatibilities between the block and
        # any other block. The block is thread incompatible with another block
        # if both have the same parent in their thread.
        block = self.block_dict[block_id]
        parent_in_thread = block.block_references[block.thread_id]
        children_of_parent_in_thread = [
            child for child in self.graph.successors(parent_in_thread)
            if (self.block_dict[child].thread_id
                == self.block_dict[parent_in_thread].thread_id)
            ]
        for child in children_of_parent_in_thread:
            if not child == block_id:
                # Add a direct incompatibility between "block_id" and "child"
                if self.direct_incompatibility_graph.has_node(child):
                    self.direct_incompatibility_graph.fast_add_edge(block_id,
                                                                    child)
                    self.constraint_graph.fast_add_edge(block_id, child)
                    if self.compatibility_graph.has_edge(block_id, child):
                        self.compatibility_graph.remove_edge(block_id, child)
                # Log incompatibility for statistics
                self.n_thread_incompatibilities += 1
                # Block "block_id" is indirectly incompatible with
                # descendants of "child".
                for child_desc in nx.descendants(self.graph, child):
                    if self.constraint_graph.has_node(child_desc):
                        self.constraint_graph.fast_add_edge(block_id,
                                                            child_desc)
                        if self.compatibility_graph.has_edge(block_id,
                                                             child_desc):
                            self.compatibility_graph.remove_edge(block_id,
                                                                 child_desc)
        #
        # Second, we look for grandpa incompatibilities.
        # We check if block "block_id" references at least the parents
        # of blocks that reference its grand-parent or older.
        # If parent is a genesis block, then there is no grand-parent and
        # so no grandpa incompatibility.
        if self.block_dict[parent_in_thread].block_references:
            # Get grand-children of the references
            gc_set = set()
            for br in block.block_references:
                children = self.graph.successors(br)
                for c in children:
                    if self.block_dict[c].thread_id == self.block_dict[
                            br].thread_id:
                        grandchildren = self.graph.successors(c)
                        for gc in grandchildren:
                            if (self.block_dict[gc].thread_id
                                    == self.block_dict[br].thread_id):
                                gc_set.add(gc)
            # Add their descendants in same thread
            for bgc in set(gc_set):
                descendants = nx.descendants(self.graph, bgc)
                for d in descendants:
                    if self.block_dict[d].thread_id == self.block_dict[
                            bgc].thread_id:
                        gc_set.add(d)
            # For each grand-children in same thread of references, check that
            # it references at least parent_in_thread.
            for bgc in gc_set:
                parent_bg = self.block_dict[
                    bgc].block_references[block.thread_id]
                if not self.has_path_thread(parent_in_thread, parent_bg):
                    # If there is no path between the parent of "block_id" and
                    # the parent of "bgc" in the thread of
                    # "block_id", then there is a grandpa incompatibility
                    # between "block_id" and "bgc" and we add an undirected
                    # edge in the direct incompatibility and constraint graphs.
                    if self.direct_incompatibility_graph.has_node(bgc):
                        self.direct_incompatibility_graph.fast_add_edge(
                            block_id, bgc)
                        self.constraint_graph.fast_add_edge(block_id, bgc)
                        if self.compatibility_graph.has_edge(block_id, bgc):
                            self.compatibility_graph.remove_edge(block_id, bgc)
                    # Log incompatibility for statistics
                    self.n_grandpa_incompatibilities += 1
                    # Block "block_id" is also indirectly incompatible
                    # with descendants of b.
                    for b_child in nx.descendants(self.graph, bgc):
                        if self.constraint_graph.has_node(b_child):
                            self.constraint_graph.fast_add_edge(block_id,
                                                                b_child)
                            if self.compatibility_graph.has_edge(block_id,
                                                                 b_child):
                                self.compatibility_graph.remove_edge(block_id,
                                                                     b_child)
        # Finally, the block "block_id" also inherits from the
        # indirect incompatibilities of its referenced blocks.
        for block_referenced in block.block_references:
            if self.constraint_graph.has_node(block_referenced):
                for neighbor in list(
                        self.constraint_graph.neighbors(block_referenced)):
                    self.constraint_graph.fast_add_edge(block_id, neighbor)
                    if self.compatibility_graph.has_edge(block_id, neighbor):
                        self.compatibility_graph.remove_edge(block_id,
                                                             neighbor)

    def update_blockclique(self, block_id):
        """Update the cliques of compatible blocks and the blockclique.

        If the block has no direct incompatibilities, it will not create
        new cliques so we can update cliques incrementally.
        Otherwise we have to recompute all cliques.

        :param block_id: The id of the block being processed
        """
        # Get the set of direct incompatibilities of block "block_id"
        direct_incompatibilities = set(
            self.direct_incompatibility_graph.neighbors(block_id)
            )
        if direct_incompatibilities:
            # Recompute all maximal cliques of the compatibility graph.
            # This operation takes exponential time with the number of blocks
            # in the compatibility graph, but less than 50ms for
            # less than a hundred blocks.
            self.cliques = [
                set(clique)
                for clique in nx.find_cliques(self.compatibility_graph)]
        else:
            # Update cliques incrementally
            #
            # Get the set of indirect incompatibilities of block "block_id"
            indirect_incompatibilities = set(
                self.constraint_graph.neighbors(block_id))
            for clique in self.cliques:
                # Get the set of indirect incompatibilities of block "block_id"
                # in clique "clique".
                incompatibilities_in_clique = set.intersection(
                    clique, indirect_incompatibilities)
                if not incompatibilities_in_clique:
                    # If the block is compatible with all blocks of this
                    # clique, then we add "block_id" to this clique.
                    clique.add(block_id)
        # Now that we updated the set of cliques of compatible blocks,
        # we can compute the blockclique: the clique of maximum work sum,
        # and if tie, of minimum hash sum.
        best_i = 0
        best_worksum = sum(1. / self.block_dict[block_id].target
                           for block_id in self.cliques[0])
        best_hashsum = None
        for i in range(1, len(self.cliques)):
            worksum_i = sum(1. / self.block_dict[block_id].target
                            for block_id in self.cliques[i])
            if worksum_i > best_worksum:
                best_i = i
                best_worksum = worksum_i
                best_hashsum = None
            elif worksum_i == best_worksum:
                if best_hashsum is None:
                    best_hashsum = sum(self.block_dict[block_id].block_hash
                                       for block_id in self.cliques[best_i])
                hashsum_i = sum(self.block_dict[block_id].block_hash
                                for block_id in self.cliques[i])
                if hashsum_i < best_hashsum:
                    best_i = i
                    best_worksum = worksum_i
                    best_hashsum = hashsum_i
        self.blockclique = set(self.cliques[best_i])

    def update_references(self):
        """Update the block references.

        The new references are the last block of each thread in the
        updated blockclique. To find the last block of each thread
        we search the incremental graph in topological order.
        """
        self.current_references = [-1] * self.n_threads
        for block_id in self.tsort_incr_graph:
            if block_id in self.blockclique:
                thread_id = self.block_dict[block_id].thread_id
                self.current_references[thread_id] = block_id
        # If a thread is not represented in the blockclique, we put the last
        # block of this thread removed from the incremental graph
        # as a final block.
        for thread in range(self.n_threads):
            if self.current_references[thread] == -1:
                self.current_references[thread] = \
                    self.last_removed_final[thread]

    def reduce_incremental_graph(self, block_id):
        """Remove blocks from the incremental graph.

        :param block_id: The id of the block being processed
        """
        # Keep only cliques that have strictly more blocks than the number of
        # blocks in the updated blockclique minus the finality parameter.
        n_cliques = len(self.cliques)
        n_min_blocks = len(self.blockclique) - self.finality + 1
        self.cliques = [clique
                        for clique in self.cliques
                        if len(clique) >= n_min_blocks]
        # If we removed at least one clique, we will then remove from the
        # incremental graphs all blocks that are not at least in one clique:
        # those blocks are then considered stale blocks.
        if len(self.cliques) < n_cliques:
            blocks_to_keep = set.union(*self.cliques)
            stale_blocks = set.difference(
                set(self.tsort_incr_graph),
                blocks_to_keep)
        else:
            stale_blocks = set()
        # Get the blocks that are common to all remaining cliques
        blocks_common = set.intersection(*self.cliques)
        # Update the set of descendants of each node
        for thread_id in range(self.n_threads):
            anc = self.block_dict[block_id].block_references[thread_id]
            while anc in self.tsort_incr_graph:
                self.incremental_graph.nodes[anc]["descendants"].add(block_id)
                if self.block_dict[anc].block_references:
                    anc = self.block_dict[anc].block_references[thread_id]
                else:
                    break
        # Get the blocks common to all cliques that have strictly more than
        # finality descendants.
        blocks_finality_descendants = {
            bc for bc in blocks_common
            if (len(self.incremental_graph.nodes[bc]["descendants"])
                > self.finality)
            }
        # Find common blocks that have strictly more than finality descendants
        # in at least one clique: those are the final blocks.
        final_blocks = set()
        for bfd in blocks_finality_descendants:
            for clique in self.cliques:
                if len(set.intersection(
                        clique,
                        self.incremental_graph.nodes[bfd]["descendants"])
                       ) > self.finality:
                    final_blocks.add(bfd)
                    break
        self.last_stale_blocks = stale_blocks
        # Remove the final blocks from the cliques and the blockclique
        if final_blocks:
            for i in range(len(self.cliques)):
                self.cliques[i] = set.difference(set(self.cliques[i]),
                                                 final_blocks)
            self.blockclique = set.difference(self.blockclique,
                                              final_blocks)
        # Remove stale and final blocks from the incremental graphs,
        # in topological order.
        final_and_stale_blocks = set.union(stale_blocks, final_blocks)
        for b_id in list(self.tsort_incr_graph):
            if b_id in final_and_stale_blocks:
                self.direct_incompatibility_graph.remove_node(b_id)
                self.constraint_graph.remove_node(b_id)
                self.compatibility_graph.remove_node(b_id)
                self.incremental_graph.remove_node(b_id)
                self.tsort_incr_graph.remove(b_id)
                if b_id in final_blocks:
                    self.last_removed_final[
                        self.block_dict[b_id].thread_id] = b_id
                    # Update the last cross references between threads.
                    if self.block_dict[b_id].block_references:
                        for thread in range(self.n_threads):
                            b_thread = self.block_dict[b_id].thread_id
                            self.cross_refs[b_thread][thread] = \
                                self.block_dict[b_id].block_references[thread]
        # We also directly remove stale blocks from self.graph
        for b_stale in stale_blocks:
            self.graph.remove_node(b_stale)
            self.tsort_graph.remove(b_stale)
        # The following is only used for statistics
        for b_id in final_blocks:
            if self.node_id == -1:
                # Add timestamp to removed block
                self.block_dict[b_id].timestamp_removed_final = (
                    self.block_dict[block_id].timestamp
                    )
                self.block_dict[b_id].status = "final"
                self.n_processed_transactions += \
                    self.block_dict[block_id].n_transactions
        for b_id in stale_blocks:
            if self.node_id == -1:
                self.block_dict[b_id].status = "stale"
        # Compute the number of cliques, final and stale blocks for statistics
        self.n_cliques = len(self.cliques)
        self.n_final_blocks += len(final_blocks)
        self.n_stale_blocks += len(stale_blocks)
        # Return and say if some blocks were removed from the incremental graph
        return len(final_blocks) > 0

    def reduce_graph(self):
        """Remove blocks that are not needed anymore.

        In order to save RAM in this simulation and disk space in real,
        we remove final blocks that have no chance to be used anymore in
        the future computations of the blockclique.
        We could keep only the last say 1,000 blocks, but here as
        we simulate thousands of nodes, we need to do something
        more precise and keep only the blocks that have a chance
        to be used: about 300 blocks with T=32 and F=32.

        We remove final blocks that are older in topological order than the
        oldest last cross reference between two final blocks of any threads,
        and that don't have a child in their thread that is referenced by
        a block that can still be the grand-parent of a future block.
        Indeed, a new block must have more recent references than its parent,
        and thus can't reference a block older than the blocks referenced
        by the last final block of its thread, and blocks that have a child
        that is referenced by a potential grandpa can be needed to compute
        grandpa incompatibilities in the future.
        """
        # Get the blocks referenced by the last final block of any thread
        last_cross_ref = {self.cross_refs[thread1][thread2]
                          for thread1 in range(self.n_threads)
                          for thread2 in range(self.n_threads)}
        # Get the set of last cross references plus incremental graph blocks
        blocks_limit = set.union(last_cross_ref, self.tsort_incr_graph)
        # Get the blocks that are older in topological order than any last
        # cross reference and any block of the incremental graph.
        block_to_remove = set()
        for block_id in self.tsort_graph:
            if block_id in blocks_limit:
                break
            else:
                block_to_remove.add(block_id)
        # Find potential future grand-parents
        potential_grandpa = set()
        for block_id in set.union(
                set(self.tsort_incr_graph),
                {b for b in self.last_removed_final if b >= 0}):
            if self.block_dict[block_id].block_references:
                # The parents of a non final block can be grand-parent of a
                # new block.
                parent = self.block_dict[block_id].block_references[
                    self.block_dict[block_id].thread_id]
                potential_grandpa.add(parent)
                # The grand-parents of a non final block can be grand-parents
                # of a block thread incompatible with this non final block.
                if self.block_dict[parent].block_references:
                    potential_grandpa.add(
                        self.block_dict[parent].block_references[
                            self.block_dict[block_id].thread_id]
                        )
        # We do not remove any potential grandpa
        block_to_remove = set.difference(block_to_remove, potential_grandpa)
        # Check that the blocks to remove don't have a child in
        # their thread that references a potential grandpa.
        for block_id in list(block_to_remove):
            remove = True
            for child in self.graph.successors(block_id):
                if self.block_dict[child].thread_id == self.block_dict[
                        block_id].thread_id:
                    for thread in range(self.n_threads):
                        if self.block_dict[child].block_references[
                                thread] in potential_grandpa:
                            remove = False
                            break
                if not remove:
                    block_to_remove.remove(block_id)
                    break
        # Remove blocks
        for b_final in block_to_remove:
            self.graph.remove_node(b_final)
            self.tsort_graph.remove(b_final)
        self.last_final_blocks = block_to_remove

    def plot(self, ax):
        """Plot the incremental block graph."""
        ax.clear()
        # Convert to an undirected graph for speed
        # graph = self.incremental_graph
        graph = nx.Graph(self.incremental_graph)
        # X-axis is the block timestamp
        pos = {}
        pos2 = {}
        for b_id in graph.nodes():
            pos[b_id] = [self.block_dict[b_id].timestamp,
                         self.block_dict[b_id].thread_id]
            pos2[b_id] = [self.block_dict[b_id].timestamp,
                          self.block_dict[b_id].thread_id]
        # Plot a line for each thread
        for thread in range(self.n_threads):
            ax.plot([min([p[0] for p in pos.values()]),
                     max([p[0] for p in pos.values()])],
                    [thread, thread], color="gray", lw=2, alpha=0.3, zorder=0)
        # Plot block references
        nx.draw_networkx_edges(graph, pos=pos, alpha=0.2,
                               width=2, edge_color="#0087e5",
                               arrow=True, ax=ax)
        # Draw references of last block
        nx.draw_networkx_edges(graph, pos=pos, alpha=1,
                               edgelist=[e for e in graph.edges
                                         if e[1] == max(graph.nodes())],
                               edge_color="#0087e5", width=3,
                               arrow=True, ax=ax)
        # Plot blocks in blockclique in orange
        nx.draw_networkx_nodes(
            graph, pos=pos, with_labels=False,
            nodelist=set(n for n in pos if n in self.blockclique),
            node_size=330, node_shape='s', node_color="#0087e5",
            alpha=1.0, ax=ax)
        # Plot blocks not in blockclique in green
        nx.draw_networkx_nodes(
            graph, pos=pos,
            nodelist=set(n for n in pos if n not in self.blockclique),
            alpha=1.0, node_shape='s', node_size=330, node_color="#FF8C00",
            ax=ax)
        # Plot block ids
        nx.draw_networkx_labels(self.incremental_graph, pos=pos2,
                                font_color="white", ax=ax)
        # Plot extras
        xmin = min([p[0] for p in pos.values()])
        xmax = max([p[0] for p in pos.values()])
        ax.set_xlim([xmin - 0.05 * (xmax - xmin), xmax + 0.05 * (xmax - xmin)])
        ax.set_ylim([-0.4, self.n_threads - 1. + 0.4])
        plt.xticks(fontsize=14)
        plt.yticks(fontsize=14)
        plt.xlabel("Time (sec)", fontsize=20)
        plt.ylabel("Threads", fontsize=20)
        plt.gca().invert_yaxis()
