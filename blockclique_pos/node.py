# -*- coding: utf-8 -*-
"""
Implementation of mining nodes.

"""
import numpy as np

from blockclique_pos.block import Block
from blockclique_pos.block_graph import BlockGraph


class Node(object):
    """Mining Node."""
    def __init__(self, node_id, n_threads, n_nodes, time_between_blocks,
                 finality, genesis_block_dict,
                 mining_power=1000., attacker=None):
        """Create a mining node.

        :param node_id: Identifier of the node
        :param n_threads: Number of threads
        :param n_nodes: Number of nodes
        :param time_between_blocks: Average Time between two blocks in a thread
        :param finality: Finality parameter
        :param genesis_block_dict: Initial block dictionary
        :param transaction_pool: Transaction pool
        """
        self.node_id = node_id
        self.n_threads = n_threads
        self.n_nodes = n_nodes
        self.time_between_blocks = time_between_blocks
        self.finality = finality
        self.attacker = attacker
        # Assign node to a thread (deterministic here, but random in general)
        self.thread = self.node_id % self.n_threads
        # Initialize time when a block is found by Node
        self.time_next_block = 0.
        # Initialize block graph
        self.block_graph = BlockGraph(self.node_id, self.n_threads,
                                      self.thread, genesis_block_dict,
                                      self.time_between_blocks, self.finality)
        # Initialize references
        self.block_references = range(self.n_threads)
        # Initialize mining
        if self.attacker and self.thread == 0 and self.node_id == 0:
            p = self.attacker["p"]
            if self.thread == 0 and self.node_id == 0:
                self.mining_power = mining_power * self.n_nodes * p
            else:
                self.mining_power = (mining_power * (1-p)
                                     * self.n_nodes / (self.n_nodes - 1))
        else:
            self.mining_power = mining_power
        self.next_block_target = 1.
        self.created_blocks = {}  # [thread][generation]
        for thread in range(self.n_threads):
            self.created_blocks[thread] = {}
        self.produce_blocks(0., genesis_block_dict)
        # Total rewards received (used for statistics)
        self.rewards = 0.

    def compute_reward(self, generation):
        """Compute the reward of a block.

        The reward scheme is here a fixed total supply of 20,000,000 tokens
        with half of the remaining tokens created every 10 years
        :param generation: Generation of the block
        """
        r1 = 0.0438741
        q = 0.99999993
        return int(r1 * q ** (generation - 1) * 10. ** 8.) * 10. ** (- 8.)

    def process_block(self, time_reception, block, block_dict):
        """Process a received block.

        :param time_reception: Time of reception
        :param block_id: Identifier of the block
        :param block_dict: Shared dictionary of blocks
        :param transaction_pool: Transaction pool
        """
        # Update block references
        self.block_references = self.block_graph.update(block.block_id)
        return self.produce_blocks(time_reception, block_dict)

    def produce_blocks(self, timestamp, block_dict):
        """Produce blocks up to a given timestamp.

        :param timestamp:
        :param block_dict:
        """
        self.time_next_block = np.inf
        block_list = []
        for block_id in self.block_references:
            if self.node_id in block_dict[block_id].next_bakers:
                if (block_dict[block_id].generation + 1 not in
                        self.created_blocks[block_dict[block_id].thread_id]):
                    priority = block_dict[block_id].next_bakers.index(
                        self.node_id)
                    time_produce = (block_dict[block_id].timestamp +
                                    (1 + priority) * self.time_between_blocks)
                    if time_produce <= timestamp:
                        block_list.append(
                            self.create_block(time_produce,
                                              block_dict[block_id].thread_id,
                                              block_dict, priority))
                    else:
                        self.time_next_block = min(self.time_next_block,
                                                   time_produce)
        return block_list

    def create_block(self, timestamp, thread_id, block_dict, priority=0):
        """Create a block.

        :param block_id: Identifier of the new block
        :param timestamp: Timestamp of the new block
        :param block_dict: Shared dictionary of blocks
        """
        # Compute block generation (incremental)
        generation = (block_dict[self.block_references[thread_id]].generation
                      + 1)
        # Compute block reward based on generation
        reward = self.compute_reward(generation)
        # Put a random hash, we do not simulate them in detail here
        block_hash = np.random.random()
        # Compute list of next bakers
        np.random.seed(thread_id * generation)
        next_bakers = list(np.random.choice(range(self.n_nodes), size=5))
        self.created_blocks[thread_id][generation] = True
        # Return the newly created block
        return Block(thread_id, timestamp, self.block_references,
                     self.node_id, generation,
                     reward, block_hash, priority, np.random.random(),
                     next_bakers=next_bakers)
