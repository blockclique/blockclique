# -*- coding: utf-8 -*-
"""
Implementation of blockclique threads in dedicated processes.

"""
from multiprocessing import Process

import numpy as np

from blockclique_pos.node import Node


class Thread(Process):
    """Implementation of a blockclique thread"""
    def __init__(self, thread, trial, n_threads, n_nodes, time_between_blocks,
                 max_transactions_per_block, average_transactions_per_second,
                 transaction_size, block_header_size, finality,
                 child_conn, instruction_queue, block_dict, attacker=None):
        """A Thread runs its associated mining nodes and transaction pool.

        :param thread_id: Identifier of this thread
        :param trial: Identifier of the experiment
        :param n_threads: Number of threads
        :param n_nodes: Number of nodes
        :param time_between_blocks: Average time between two blocks of a thread
        :param max_transactions_per_block:
        :param average_transactions_per_second: (transaction per second)
        :param transaction_size: (bit)
        :param block_header_size: (bit)
        :param finality:
        :param child_conn: The child side of a Pipe to talk with Experiment.
        :param instruction_queue: A queue to get instructions from Experiment.
        """

        Process.__init__(self)

        self.thread = thread
        self.trial = trial
        self.n_threads = n_threads
        self.n_nodes = n_nodes
        self.time_between_blocks = time_between_blocks
        self.max_transactions_per_block = max_transactions_per_block
        self.average_transactions_per_second = average_transactions_per_second
        self.transaction_size = transaction_size
        self.block_header_size = block_header_size
        self.finality = finality
        self.child_conn = child_conn
        self.instruction_queue = instruction_queue
        self.block_dict = block_dict
        self.attacker = attacker
        # Initialize numpy random generator based on trial, thread and finality
        np.random.seed(10000 * self.thread
                       + 100 * self.trial
                       + self.finality)
        # List of ids of nodes associated to this thread
        self.node_ids = [node_id for node_id in range(self.n_nodes)
                         if node_id % self.n_threads == self.thread]
        # Create Nodes
        self.nodes = {}
        for node_id in self.node_ids:
            self.nodes[node_id] = Node(
                node_id,
                self.n_threads,
                self.n_nodes,
                self.time_between_blocks,
                self.finality,
                self.block_dict,
                attacker=self.attacker
                )
        #
        self.pending_blocks = []
        # Start process
        self.daemon = True
        self.start()

    def get_info_next_block(self):
        """Find the node that will create next block."""
        best_node = self.node_ids[0]
        best_time = self.nodes[self.node_ids[0]].time_next_block
        for node_id in self.nodes:
            if self.nodes[node_id].time_next_block < best_time:
                best_node = node_id
                best_time = self.nodes[node_id].time_next_block
        return best_node, best_time

    def sample_stake(self, timestamp):
        """Sample the next nodes that have a right to stake."""
        shift = self.time_between_blocks
        for node_id in np.random.permutation(self.node_ids):
            self.nodes[node_id].time_next_block = timestamp + shift
            shift += self.time_between_blocks

    def run(self):
        """Run the process loop."""
        while True:
            # Wait for an instruction from the experiment
            (action, args) = self.instruction_queue.get()
            # Check instruction
            if action == "process_block":
                # Node "node_id" receives block "block" at time "time_event"
                node_id, time_event, block = args
                # Put the block in the block dict in case it is not
                self.block_dict[block.block_id] = block
                # Node "node_id" processes the block
                block_list = self.nodes[node_id].process_block(
                    time_event,
                    block,
                    self.block_dict
                    )
                self.pending_blocks = self.pending_blocks + block_list

            elif action == "get_pending_blocks":
                self.child_conn.send(self.pending_blocks)
                self.pending_blocks = []

            elif action == "create_block":
                # Node "node_id" creates block "block_id" at time "time_block"
                timestamp, = args
                for node_id in self.node_ids:
                    if self.nodes[node_id].time_next_block == timestamp:
                        self.pending_blocks = (
                            self.pending_blocks +
                            self.nodes[node_id].produce_blocks(
                                timestamp,
                                self.block_dict))
                # Get info about next block
                best_node, best_time = self.get_info_next_block()
                # Send back the created block and info about next block
                self.child_conn.send((self.pending_blocks,
                                      best_node,
                                      best_time))
                self.pending_blocks = []

            elif action == "get_info_next_block":
                best_node, best_time = self.get_info_next_block()
                self.child_conn.send((best_node, best_time))

            elif action == "ping":
                self.child_conn.send(True)

            else:
                raise NotImplementedError
