# -*- coding: utf-8 -*-
"""
Implementation of an experiment with the blockclique.

"""
from __future__ import print_function

from multiprocessing import Pipe, Queue

import time
import heapq
import numpy as np
import matplotlib.pyplot as plt

from blockclique_pos.block import Block
from blockclique_pos.thread import Thread
from blockclique_pos.network import Network
from blockclique_pos.block_graph import BlockGraph
from blockclique_pos.transaction_pool import TransactionPool


class Experiment(object):
    """Experiment with the blockclique"""
    def __init__(self, trial, n_threads, n_nodes, time_between_blocks,
                 max_transactions_per_block, average_transactions_per_second,
                 block_verification_time, transaction_verification_time,
                 transaction_size, block_header_size, average_bandwidth,
                 average_latency, finality, draw_graph=False, real_time=False,
                 attacker=None, log=True):
        """Setup an experiment.

        :param trial: Identifier of this independent experiment
        :param n_threads: Number of threads
        :param n_nodes: Number of nodes
        :param time_between_blocks: Average time between two blocks of a thread
        :param max_transactions_per_block:
        :param average_transactions_per_second: (transaction per second)
        :param block_verification_time: (second)
        :param transaction_verification_time: (second)
        :param transaction_size: (bit)
        :param block_header_size: (bit)
        :param average_bandwidth: (bit per second)
        :param average_latency: (second)
        :param finality:
        :param draw_graph: If the incremental graph is drawn
        :param real_time: If the experiments runs at real time
        """
        self.trial = trial
        self.n_threads = n_threads
        self.n_nodes = n_nodes
        self.time_between_blocks = time_between_blocks
        self.max_transactions_per_block = max_transactions_per_block
        self.average_transactions_per_second = average_transactions_per_second
        self.block_verification_time = block_verification_time
        self.transaction_verification_time = transaction_verification_time
        self.transaction_size = transaction_size
        self.block_header_size = block_header_size
        self.average_bandwidth = average_bandwidth
        self.average_latency = average_latency
        self.finality = finality
        self.draw_graph = draw_graph
        self.real_time = real_time
        self.attacker = attacker
        self.log = log
        # Initialize numpy random generator based on trial number
        np.random.seed(self.trial)
        # Initialize simulated time
        self.time = 0.
        # Get the real starting time of the experiment
        self.start_time = time.time()
        # Initialize a dictionary of blocks
        self.block_dict = {}
        # Create one genesis block per thread
        for thread_id in range(self.n_threads):
            next_bakers = list(np.random.choice(range(self.n_nodes), size=5))
            block = Block(thread_id,
                          (float(thread_id) * time_between_blocks
                           / float(n_threads)),
                          [], -1, 0, 0, 0., 0, np.random.random(),
                          block_id=thread_id, next_bakers=next_bakers)
            self.block_dict[thread_id] = block
        # Initialize the (node_id, time) of the next block found in each thread
        self.next_block = [(0, 0)] * self.n_threads
        # Initialize a list of parent connections through a Pipe to each thread
        self.parent_conns = []
        # Initialize a list of Queues to send instructions to each thread
        self.instruction_queues = []
        # Initialize the threads
        self.thread_processes = []
        for thread_id in range(self.n_threads):
            # Open a two-way pipe with this thread
            parent_conn, child_conn = Pipe()
            # Open a queue to send instruction to this thread
            instruction_queue = Queue()
            # Add the parent side of the pipe connection to the list
            self.parent_conns.append(parent_conn)
            # Add the queue to the queue list
            self.instruction_queues.append(instruction_queue)
            # Spawn a blockclique thread in a dedicated process
            self.thread_processes.append(
                Thread(thread_id, trial, n_threads, n_nodes,
                       time_between_blocks, max_transactions_per_block,
                       average_transactions_per_second, transaction_size,
                       block_header_size, finality,
                       child_conn, instruction_queue, self.block_dict,
                       self.attacker)
                )
            # Ask the thread to send back info about its next block
            self.instruction_queues[thread_id].put(("get_info_next_block", []))
            self.next_block[thread_id] = parent_conn.recv()
        # Create peer-to-peer network
        self.network = Network(self.n_nodes,
                               self.block_verification_time,
                               self.transaction_verification_time,
                               self.transaction_size,
                               self.block_header_size,
                               self.average_bandwidth,
                               self.average_latency,
                               self.attacker)
        # The following is only used for statistics
        # Initialize a block graph just for statistics
        self.block_graph = BlockGraph(-1,
                                      self.n_threads,
                                      -1,
                                      self.block_dict,
                                      self.time_between_blocks,
                                      self.finality)
        # Initialize transaction pools
        self.transaction_pools = []
        for thread in range(self.n_threads):
            self.transaction_pools.append(TransactionPool(
                thread,
                self.n_threads,
                self.max_transactions_per_block,
                self.average_transactions_per_second
                ))
        # Initialize the average throughput
        self.average_throughput = 0.
        # Initialize the time spent to run the experiment
        self.simu_time = 0.
        # Initialize a dictionary of statistics results
        self.stats = {}
        self.stats["interrupted"] = False
        # Prepare a matplotlib figure for drawing the incremental graph
        if self.draw_graph:
            plt.figure(figsize=(16, 9))
            plt.ion()
            plt.show()
            self.texts = []

    def fetch_info_next_block(self):
        """Ask threads for their next block discovery."""
        for thread_id in range(self.n_threads):
            self.instruction_queues[thread_id].put(("get_info_next_block", []))
        for thread_id in range(self.n_threads):
            self.next_block[thread_id] = self.parent_conns[thread_id].recv()

    def get_info_next_block(self):
        """Find info about the next block discovery."""
        best_node = self.next_block[0][0]
        best_time = self.next_block[0][1]
        for thread_id in range(1, self.n_threads):
            if self.next_block[thread_id][1] < best_time:
                best_node = self.next_block[thread_id][0]
                best_time = self.next_block[thread_id][1]
        return best_node, best_time

    def update_nodes(self, time_next_block):
        """Transmit blocks.

        Transmit blocks in the peer-to-peer network and
        process received blocks, up to time "time_next_block".

        :param time_next_block:
        """
        while self.network.events_queue:
            # Get the next event
            (time_event, event) = self.network.events_queue[0]
            # If the event is after "time_next_block",
            # we don't pop it and we stop transmitting blocks for now.
            if time_event > time_next_block:
                break
            # Else, we pop the event
            heapq.heappop(self.network.events_queue)
            # Check the next event
            if event[0] == "receive_block":
                # Node "node_id" receives block "block_id"
                node_id, block_id, n_transactions = event[1:]
                # The node starts to broadcast the block to its connected peers
                self.network.broadcast_block(
                    time_event,
                    node_id,
                    block_id,
                    n_transactions
                    )
                # If the node is not a relay node, it processes the block
                if node_id < self.n_nodes:
                    # We send the instruction to process the block
                    # to the corresponding thread process.
                    self.instruction_queues[node_id % self.n_threads].put(
                        ("process_block",
                         [node_id, time_event, self.block_dict[block_id]]))
            elif event[0] == "recv_msg_new_block_available":
                # Node "node_to" receives message 'new block available'
                node_from, node_to, block_id, n_transactions = event[1:]
                self.network.recv_msg_new_block_available(
                    time_event, node_from, node_to, block_id, n_transactions)
            elif event[0] == "recv_msg_give_me_block":
                # Node "node_to" receives message 'give me block'
                node_from, node_to, block_id, n_transactions = event[1:]
                self.network.recv_msg_give_me_block(
                    time_event, node_from, node_to, block_id, n_transactions)
            elif event[0] == "send_blocks":
                # Node "node_from" continues to send queued block
                node_from, = event[1:]
                self.network.send_blocks(time_event, node_from)
        # If there are no more events before "time_next_block",
        # then the simulation time becomes "time_next_block".
        if time_next_block > self.time:
            self.time = time_next_block

    def run(self, duration):
        """Run the experiment.

        Run the experiment for a simulated time specified by "duration".

        :param duration:
        """
        # Get the real starting time of the experiment
        time_start_run = time.time()
        # Fetch info from threads about next block
        self.fetch_info_next_block()
        # While we do not reach the target simulated duration,
        # we find who creates the next block, transmit blocks up to the time
        # of the next block, and compute some statistics.
        while self.time < duration:
            if self.real_time:
                while time.time() - self.start_time < self.time:
                    time.sleep(0.01)
            #
            self.fetch_info_next_block()
            # Find the node and time of the next block discovery
            node_id, time_next_block = self.get_info_next_block()
            # Transmit blocks in the peer-to-peer network
            # up to the time of the next block discovery.
            self.update_nodes(time_next_block)
            # Node "node_id" creates the next block "block_id".
            # The ids of blocks are used in this simulation as a human readable
            # block order, but they are never required by the nodes to compute
            # the blockclique or anything else, where they can use the block
            # hash instead as the identifier.
            thread = node_id % self.n_threads
            # Send the create block instruction to the thread of "node_id"
            self.instruction_queues[thread].put(("create_block",
                                                 [time_next_block, ]))
            # Wait for the thread to send back the new block
            blocks, best_node, best_time = self.parent_conns[thread].recv()
            self.next_block[thread] = (best_node, best_time)
            # Get pending blocks
            pending_blocks = []
            for thread_id in range(self.n_threads):
                self.instruction_queues[thread_id].put(
                    ("get_pending_blocks", []))
            for thread_id in range(self.n_threads):
                pending_blocks = (pending_blocks +
                                  self.parent_conns[thread_id].recv())
            # Fill in block fields
            for block in blocks + pending_blocks:
                node_id = block.node_id
                block_id = len(self.block_dict)
                block.n_transactions = self.transaction_pools[
                    block.thread_id].get_transactions(
                        self.time, block.block_references[block.thread_id])
                block.block_id = block_id
                #
                self.transaction_pools[block.thread_id].update_block(block)
                # The new block is added to the block dictionary
                self.block_dict[block_id] = block
                # Send block back to creator
                self.instruction_queues[node_id % self.n_threads].put(
                    ("process_block",
                     [node_id, self.time, block]))
                # We log that node "node_id" received block "block_id"
                self.network.network_graph.nodes[node_id][
                    "blocks_asked"][block_id] = True
                # Node "node_id" starts to broadcast the block
                # at the time it created it
                self.network.broadcast_block(self.time,
                                             node_id, block_id,
                                             block.n_transactions,
                                             creator=True)
                # The following is only used for statistics and drawing.
                # We compute the blockclique and the best block references
                # from the point of view of an oracle that sees all
                # blocks as soon as they are created.
                best_refs = self.block_graph.update(block_id)
                # Compute the average throughput
                self.average_throughput = 0
                for thread_id in range(self.n_threads):
                    self.average_throughput += self.transaction_pools[
                        thread_id].processed_transactions[best_refs[thread_id]]
                self.average_throughput = int(self.average_throughput /
                                              (self.time -
                                               (self.time_between_blocks -
                                                self.time_between_blocks /
                                                float(self.n_threads))))
                # Print statistics
                if self.log:
                    print("Simu Time: %.2f sec." % self.time)
                    print("Block", block_id, "baked by node", node_id, "in thread",
                          block.thread_id, "at timestamp", block.timestamp,
                          "with parent", block.block_references[block.thread_id])
                    print("Txs:", block.n_transactions, "  Reward:", block.reward,
                          "  Hash:", block.block_hash)
                    print("G size:", len(self.block_graph.graph), "   GC size:",
                          len(self.block_graph.constraint_graph))
                    print("Cliques:", len(self.block_graph.cliques),
                          "  Stale blocks: {}".format(
                              self.block_graph.n_stale_blocks)
                          + (" ({:.1f} %)".format(
                              100.*self.block_graph.n_stale_blocks
                              / float(self.block_graph.n_stale_blocks
                                      + self.block_graph.n_final_blocks)
                              )
                             if (self.block_graph.n_stale_blocks
                                 + self.block_graph.n_final_blocks > 0) else ""),
                          "thread:", self.block_graph.n_thread_incompatibilities,
                          "grandpa:", self.block_graph.n_grandpa_incompatibilities)
                    print("Average Transaction Throughput:",
                          self.average_throughput, "txps")
                    print()
            if len(self.block_graph.blockclique) < 0.6 * len(
                    self.block_graph.incremental_graph):
                print("Stop experiment: len(self.block_graph.blockclique)=",
                      len(self.block_graph.blockclique),
                      "len(self.block_graph.incremental_graph)=",
                      len(self.block_graph.incremental_graph))
                self.stats["interrupted"] = True
                break
            # Plot statistics on figure
            if self.draw_graph:
                self.block_graph.plot()
                for txt in self.texts:
                    txt.remove()
                self.texts = []
                fontsize = plt.gcf().get_size_inches()[0]
                self.texts.append(plt.gcf().text(
                    0.14, 0.92,
                    "Simu Time:   {:.2f} sec\nThroughput:  {:4d} txps".format(
                        self.time, int(self.average_throughput),
                        self.block_graph.n_stale_blocks),
                    fontsize=fontsize))
                self.texts.append(plt.gcf().text(
                    0.35, 0.92,
                    "Final blocks: {}\nStale blocks: {}".format(
                        self.block_graph.n_final_blocks,
                        self.block_graph.n_stale_blocks)
                    + (" ({:.1f} %)".format(
                        100.*self.block_graph.n_stale_blocks
                        / float(self.block_graph.n_stale_blocks
                                + self.block_graph.n_final_blocks)
                        )
                       if (self.block_graph.n_stale_blocks
                           + self.block_graph.n_final_blocks > 0) else ""),
                    fontsize=fontsize))
                self.texts.append(plt.gcf().text(
                   0.55, 0.9,
                    "G size:   {:3d}\nGC size: {:3d}\nCliques: {:3d}".format(
                        len(self.block_graph.graph),
                        len(self.block_graph.constraint_graph),
                        len(self.block_graph.cliques)),
                    fontsize=fontsize))
                self.texts.append(plt.gcf().text(
                    0.7, 0.92,
                    "Block: {:4d}    Thread: {:3d}\n".format(
                        block_id, node_id % self.n_threads)
                    + "Txs:  {:4d}    Miner: {:4d}".format(
                        int(block.n_transactions), node_id),
                    fontsize=fontsize))
                # Draw the figure
                plt.draw()
                plt.pause(0.001)
                # plt.waitforbuttonpress()  # Plot blocks one by one
        # Log the time needed to run the experiment
        self.simu_time = time.time() - time_start_run

    def compute_stats(self):
        """Compute additional statistics after the end of the experiment."""
        self.stats["Number of Blocks"] = len(self.block_dict)
        self.stats["Number of Stale Blocks"] = self.block_graph.n_stale_blocks
        self.stats["Average Transaction Throughput"] = self.average_throughput
        self.stats["Average Confirmation Time"] = np.mean(
            [self.block_dict[block_id].timestamp_removed_final
             - self.block_dict[block_id].timestamp
             for block_id in self.block_dict
             if self.block_dict[block_id].timestamp_removed_final
             is not None]
            )
        self.stats["Stddev Confirmation Time"] = np.std(
            [self.block_dict[block_id].timestamp_removed_final
             - self.block_dict[block_id].timestamp
             for block_id in self.block_dict
             if self.block_dict[block_id].timestamp_removed_final
             is not None]
            )
        self.stats["Simulation Time"] = self.simu_time
        # Statistics of the imbalance attack
        if self.attacker:
            # Statistics of attacker
            self.stats["attacker_n_final_blocks"] = len(
                [b_id
                 for b_id in self.block_dict
                 if self.block_dict[b_id].node_id == 0
                 and self.block_dict[b_id].status == "final"])
            self.stats["attacker_n_stale_blocks"] = len(
                [b_id
                 for b_id in self.block_dict
                 if self.block_dict[b_id].node_id == 0
                 and self.block_dict[b_id].status == "stale"])
            self.stats["attacker_n_final_txs"] = sum([
                self.block_dict[b_id].n_transactions
                for b_id in self.block_dict
                if self.block_dict[b_id].node_id == 0
                and self.block_dict[b_id].status == "final"])
            # Statistics of honest nodes in the same thread as the attacker
            honest_same_thread = [node_id
                                  for node_id in range(self.n_nodes)
                                  if (node_id % self.n_threads == 0)
                                  and node_id > 0]
            self.stats["honest_same_thread_n_final_blocks"] = len(
                [b_id
                 for b_id in self.block_dict
                 if self.block_dict[b_id].node_id in honest_same_thread
                 and self.block_dict[b_id].status == "final"])
            self.stats["honest_same_thread_n_stale_blocks"] = len(
                [b_id
                 for b_id in self.block_dict
                 if self.block_dict[b_id].node_id in honest_same_thread
                 and self.block_dict[b_id].status == "stale"])
            self.stats["honest_same_thread_n_final_txs"] = sum([
                self.block_dict[b_id].n_transactions
                for b_id in self.block_dict
                if self.block_dict[b_id].node_id in honest_same_thread
                and self.block_dict[b_id].status == "final"])
            # Statistics of honest nodes not in the same thread as the attacker
            honest_other_thread = [node_id
                                   for node_id in range(self.n_nodes)
                                   if node_id % self.n_threads > 0]
            self.stats["honest_other_thread_n_final_blocks"] = len(
                [b_id
                 for b_id in self.block_dict
                 if self.block_dict[b_id].node_id in honest_other_thread
                 and self.block_dict[b_id].status == "final"])
            self.stats["honest_other_thread_n_stale_blocks"] = len(
                [b_id
                 for b_id in self.block_dict
                 if self.block_dict[b_id].node_id in honest_other_thread
                 and self.block_dict[b_id].status == "stale"])
            self.stats["honest_other_thread_n_final_txs"] = sum([
                self.block_dict[b_id].n_transactions
                for b_id in self.block_dict
                if self.block_dict[b_id].node_id in honest_other_thread
                and self.block_dict[b_id].status == "final"])
        else:
            self.stats["attacker_n_final_blocks"] = 0
            self.stats["attacker_n_stale_blocks"] = 0
            self.stats["attacker_n_final_txs"] = 0
            self.stats["honest_same_thread_n_final_blocks"] = 0
            self.stats["honest_same_thread_n_stale_blocks"] = 0
            self.stats["honest_same_thread_n_final_txs"] = 0
            self.stats["honest_other_thread_n_final_blocks"] = 0
            self.stats["honest_other_thread_n_stale_blocks"] = 0
            self.stats["honest_other_thread_n_final_txs"] = 0
