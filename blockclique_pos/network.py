# -*- coding: utf-8 -*-
"""
Implementation of a network of mining nodes.

"""
import heapq

from collections import deque

import numpy as np
import networkx as nx


class Network(object):
    """Network of mining nodes."""
    def __init__(self, n_nodes, block_verification_time,
                 transaction_verification_time, transaction_size,
                 block_header_size, average_bandwidth,
                 average_latency, attacker):
        """Create a network of mining nodes.

        :param n_nodes: Number of nodes
        :param block_verification_time: (second)
        :param transaction_verification_time: (second)
        :param transaction_size: (bit)
        :param block_header_size: (bit)
        :param average_bandwidth: (bit per second)
        :param average_latency: (second)
        """
        self.n_nodes = n_nodes
        self.block_verification_time = block_verification_time
        self.transaction_verification_time = transaction_verification_time
        self.transaction_size = transaction_size
        self.block_header_size = block_header_size
        self.average_bandwidth = average_bandwidth
        self.average_latency = average_latency
        self.attacker = attacker
        # Initialize a priority queue to handle block transmission events
        self.events_queue = []
        # Build a random peer-to-peer network of nodes
        self.network_graph = nx.DiGraph()
        # Add nodes
        for node_id in range(self.n_nodes):
            self.network_graph.add_node(node_id)
            # Initialize time when node is ready to send a block
            self.network_graph.nodes[node_id]["time_ready"] = 0.
            # Initialize time when node is verified a given block
            self.network_graph.nodes[node_id]["time_verified"] = {}
            # Initialize blocks verified by nodes
            self.network_graph.nodes[node_id]["blocks_asked"] = {}
            # Initialize a queue of blocks to send, to prioritize own blocks
            self.network_graph.nodes[node_id]["sending_queue"] = deque([])
            # Initialize a queue of messages to send
            self.network_graph.nodes[node_id]["inform_queue"] = deque([])
            # Initialize if the node is already sending blocks in event queue
            self.network_graph.nodes[node_id]["is_sending"] = False
            if self.attacker is None or node_id > 0:
                # Generate an upload bandwidth for each node (bits per second)
                # We suppose that all nodes are able to download at least at
                # the upload speed of any other node.
                self.network_graph.nodes[node_id]["bandwidth"] = (
                    self.average_bandwidth / 2.
                    + self.average_bandwidth * np.random.random())
            else:
                # The attacker has a good bandwidth
                self.network_graph.nodes[node_id]["bandwidth"] = 128000000.
        # Add edges from each node to random successors
        for node_id in range(self.n_nodes):
            n_successors = int(self.network_graph.nodes[node_id]["bandwidth"]
                               / (self.average_bandwidth / 4.))
            for _ in range(n_successors):
                node_to = np.random.choice(
                    [node_to for node_to in range(self.n_nodes)
                     if node_to != node_id])
                if self.network_graph.has_edge(node_to, node_id):
                    latency = self.network_graph[node_to][node_id]["latency"]
                else:
                    latency = self.average_latency * 2. * np.random.random()
                self.network_graph.add_edge(node_id, node_to, latency=latency)
        # Check that each node has at least one predecessor
        for node_id in range(self.n_nodes):
            if not list(self.network_graph.predecessors(node_id)):
                # If not, connect the node to its successors
                for node_to in self.network_graph.successors(node_id):
                    latency = self.network_graph[node_id][node_to]["latency"]
                    self.network_graph.add_edge(node_to, node_id,
                                                latency=latency)

    def recv_msg_new_block_available(self, time_recv_msg, node_from, node_to,
                                     block_id, n_transactions):
        """Node "node_to" receives a message that a new block is available.

        :param time_recv_msg: Time when "node_to" receives the message
        :param node_from:
        :param node_to:
        :param block_id:
        :param n_transactions:
        """
        # If "node_to" never asked for this block, it does now
        if block_id not in self.network_graph.nodes[node_to]["blocks_asked"]:
            # Get the latency of the connection
            latency = self.network_graph[node_from][node_to]["latency"]
            # Send a message asking for the block
            heapq.heappush(self.events_queue,
                           (time_recv_msg + latency,
                            ("recv_msg_give_me_block",
                             node_to, node_from,
                             block_id, n_transactions)))
            # We log that it already asked this block
            self.network_graph.nodes[node_to]["blocks_asked"][block_id] = True

    def recv_msg_give_me_block(self, time_recv_msg, node_from, node_to,
                               block_id, n_transactions):
        """Node "node_to" receives a message that "node_from" wants the block.

        :param time_recv_msg: Time when "node_to" receives the message
        :param node_from:
        :param node_to:
        :param block_id:
        :param n_transactions:
        """
        # The node receiving the message becomes the node sending the block
        node_from, node_to = node_to, node_from
        # Add this sending to the sending queue
        self.network_graph.nodes[node_from]["sending_queue"].append(
            (node_to, block_id, n_transactions))
        # If the node is node sending blocks, start to send blocks
        if not self.network_graph.nodes[node_from]["is_sending"]:
            self.send_blocks(time_recv_msg, node_from)

    def broadcast_block(self, time_broadcast, node_from,
                        block_id, n_transactions, creator=False):
        """Broadcast the block to other peers.

        At time "time_broadcast", node "node_id" checks block "block_id"
        if not already checked, then send this block to one of its connected
        peers that did not receive it yet.

        :param time_broadcast:
        :param node_id:
        :param block_id:
        :param n_transactions:
        """
        # Get the list of connected nodes in the network
        # Sort to get the same behavior in Python 2 and 3
        connected_nodes = sorted(list(
            self.network_graph.successors(node_from)))
        # Shuffle the list of connected nodes to avoid sending
        # always in the same order.
        np.random.shuffle(connected_nodes)
        # Compute the time when the node has verified the block
        # and each transaction in the block.
        if creator:
            time_verified = time_broadcast
        else:
            time_verified = (time_broadcast
                             + self.block_verification_time
                             + (n_transactions
                                * self.transaction_verification_time))
        # This node will be able to send the block as soon as verified
        self.network_graph.nodes[node_from]["time_verified"][
            block_id] = time_verified
        for node_to in connected_nodes:
            if creator:
                # Log that node_to already asked this block
                self.network_graph.nodes[node_to][
                    "blocks_asked"][block_id] = True
                # Send the block with high priority, directly without asking
                # if the node wants the block.
                self.network_graph.nodes[node_from][
                    "sending_queue"].appendleft(
                        (node_to, block_id, n_transactions))
            else:
                # Add a message to the queue of messages to send
                self.network_graph.nodes[node_from]["inform_queue"].append(
                    (node_to, block_id, n_transactions))
        # If the node is not already sending blocks, start to send
        if not self.network_graph.nodes[node_from]["is_sending"]:
            self.send_blocks(time_broadcast, node_from)

    def send_blocks(self, time_send, node_from):
        """Send one queued block.

        :param time_send:
        :param node_from:
        """
        if self.network_graph.nodes[node_from]["sending_queue"]:
            # Get one queued block to send
            (node_to, block_id, n_transactions) = \
                self.network_graph.nodes[node_from]["sending_queue"].popleft()
            # Start sending when the block is verified
            time_start_sending = max(
                time_send,
                self.network_graph.nodes[node_from]["time_verified"][block_id])
            # Get the latency of the connection
            latency = self.network_graph[node_from][node_to]["latency"]
            # Compute the duration of the transmission depending
            # on block size and sender upload bandwidth.
            duration = (
                float(self.block_header_size
                      + self.transaction_size * n_transactions)
                / self.network_graph.nodes[node_from]["bandwidth"]
                )
            # Compute the time when the block is fully transmitted
            time_reception = time_start_sending + latency + duration
            # Add the event of the successor receiving the block
            # to the event queue.
            heapq.heappush(self.events_queue,
                           (time_reception,
                            ("receive_block",
                             node_to, block_id, n_transactions)))
            if node_from < self.n_nodes:
                # As soon as the sender finished to send the block,
                # he is ready for another transmission.
                time_continue_sending = time_start_sending + duration
            else:
                # The relay network is always ready to send:
                # it sends blocks in parallel to all peers.
                time_continue_sending = time_start_sending
            # Add the event of continuing to send blocks when ready
            heapq.heappush(
                self.events_queue,
                (time_continue_sending,
                 ("send_blocks", node_from)))
            # Log that this node is currently sending blocks
            self.network_graph.nodes[node_from]["is_sending"] = True
        else:
            # This node stops sending blocks
            self.network_graph.nodes[node_from]["is_sending"] = False
        # If this node is not late to send blocks, it then send all the queued
        # messages it has to send to inform other nodes of new blocks.
        if (node_from == self.n_nodes or len(
                self.network_graph.nodes[node_from]["sending_queue"]) <= int(
                    self.network_graph.nodes[node_from]["bandwidth"]
                    / (self.average_bandwidth / 4.))):
            for _ in range(len(
                    self.network_graph.nodes[node_from]["inform_queue"])):
                (node_to, block_id, n_transactions) = self.network_graph.nodes[
                    node_from]["inform_queue"].popleft()
                # Get the latency of the connection
                latency = self.network_graph[node_from][node_to]["latency"]
                # Send a message saying that a new block is available
                heapq.heappush(
                    self.events_queue,
                    (time_send + latency,
                     ("recv_msg_new_block_available",
                      node_from, node_to,
                      block_id, n_transactions)))
