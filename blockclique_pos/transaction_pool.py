# -*- coding: utf-8 -*-
"""
Implementation of the transaction pool.

"""
import numpy as np


class TransactionPool(object):
    """Transaction Pool."""
    def __init__(self, thread, n_threads, max_transactions_per_block,
                 average_transactions_per_second):
        """Create a pool of transactions.

        :param thread: thread id of this transaction pool
        :param n_threads: Number of threads
        :param max_transactions_per_block: Max transactions per block
        :param average_transactions_per_second: Average transaction throughput
        """
        self.thread = thread
        self.n_threads = n_threads
        self.max_transactions_per_block = max_transactions_per_block
        self.average_txps = average_transactions_per_second
        # Total number of transactions that appeared in this pool
        self.observed_transactions = max_transactions_per_block
        # Last time this pool has been updated
        self.last_upd = 0.
        # Initialize the number of transactions processed
        # in a given block's thread up to this block.
        self.processed_transactions = dict()
        # The genesis block processed no transaction
        self.processed_transactions[thread] = 0

    def update_block(self, block):
        """Update the processed transaction of a new block.

        The number of transactions processed up to a new block is the one of
        its father in its thread plus the transactions in this new block.

        :param block: New block
        """
        father = block.block_references[block.thread_id]
        self.processed_transactions[block.block_id] = (
            self.processed_transactions[father] + block.n_transactions
            )

    def update_pool(self, current_time):
        """Update the transaction pool.

        :param current_time:
        """
        # Add a random number of transactions to the pool based on
        # the time elapsed since last update, with a Poisson distribution.
        self.observed_transactions += np.random.poisson(
            (current_time - self.last_upd)
            * float(self.average_txps)
            / self.n_threads
            )
        self.last_upd = current_time

    def get_transactions(self, current_time, parent):
        """Get the number of transactions of a block being mined.

        Get the number of transactions to be included in a block
        as the number of unconfirmed transactions up to its parent.

        :param current_time:
        :param parent: Parent of the block being mined
        """
        self.update_pool(current_time)
        n_transactions = min(
            self.max_transactions_per_block,
            (self.observed_transactions
             - self.processed_transactions[parent])
            )
        return n_transactions
