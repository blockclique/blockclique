#!/usr/bin/env python
# -*- coding: utf-8 -*-

from setuptools import setup

setup(
    name='blockclique',
    version='0.7.0',
    license="GNU General Public License v3 (GPLv3)",
    description="Blockclique simulations.",
    url='https://gitlab.com/blockclique/blockclique',
    author="Sébastien Forestier",
    author_email="info@blockclique.io",
    long_description=open('README.md').read(),
    install_requires=["networkx", "numpy", "matplotlib"],
    classifiers=[
        "Programming Language :: Python :: 2",
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: GNU General Public License v3 (GPLv3)",
        "Natural Language :: English",
        "Operating System :: OS Independent",
        "Topic :: Blockclique",
    ],
    packages=['blockclique_pow', 'blockclique_pos']
)
