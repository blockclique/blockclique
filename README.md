## Blockclique: scaling blockchains through transaction sharding in a multithreaded block graph

Technical Paper: [arxiv.org/pdf/1803.09029](https://arxiv.org/pdf/1803.09029)

Online Demonstration: [blockclique.io](https://blockclique.io)

Blog Introduction: [blockclique.io/blog/post/0/](https://blockclique.io/blog/post/0/)

Reddit discussion: [r/Blockclique/](https://www.reddit.com/r/Blockclique/)

### Install

```
sudo python setup.py install
```

Use "python3" if you prefer.

This should install the required packages if you don't have them: numpy, networkx and matplotlib.

### Test

To test the blockclique architecture, run the "test_pow.py" or "test_pos.py" script:

```
cd test/
python test_pow.py
```

If you open the test [script](https://gitlab.com/blockclique/blockclique/blob/master/test/test_pow.py), you can experiment with the blockclique architecture by changing any parameter, such as the number of threads, the time between two blocks, the number of nodes, etc.
