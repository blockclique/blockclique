import os
import sys
import csv
import tqdm
import datetime
import subprocess
import multiprocessing

from time import gmtime, strftime


xp = sys.argv[1]

start_date = datetime.datetime.now().strftime("%Y-%m-%d_%H-%M-%S")
pool_name = sys.argv[1]
experiment_name = start_date + '-' + pool_name
result_dir = "../results/" + experiment_name
log_dir = result_dir + "/logs"
os.makedirs(result_dir)
#os.mkdir(result_dir + "/" + "pbs")
#os.mkdir(result_dir + "/" + "logs")
#os.mkdir(result_dir + "/" + "pickle")
result_filepath = result_dir + "/results.csv"


# XP0

if xp == "xp0":
    
    n_proc = 1 # NUMBER OF XPS IN PARALLEL

    n_trials = 10
    trial_list = range(n_trials)
    n_threads_list = [16, 32, 64]
    time_between_blocks_list = [16., 32., 64.]
    block_size_ps_list = [2000000, 3000000, 4000000]
    avg_txps = 20000
    def block_size_fun(block_size_ps, n_threads, time_between_blocks):
        return int(time_between_blocks*block_size_ps/n_threads)
    n_nodes_list = [1024]
    relay_list = [None]
    average_bandwidth_list = [32000000, 64000000]
    average_latency_list = [0.1]
    finality_list = [64]

    config_list = [(trial,
                    threads,
                    block_size_fun(block_size_ps, threads, time_between_blocks),
                    time_between_blocks,
                    avg_txps,
                    n_nodes,
                    average_bandwidth,
                    average_latency,
                    finality)
                   for trial in trial_list
                   for threads in n_threads_list
                   for time_between_blocks in time_between_blocks_list
                   for block_size_ps in block_size_ps_list
                   for n_nodes in n_nodes_list
                   for average_bandwidth in average_bandwidth_list
                   for average_latency in average_latency_list
                   for finality in finality_list]

elif xp == "xp_nodes":
    
    n_proc = 1 # NUMBER OF XPS IN PARALLEL
    
    n_trials = 10
    offset_trials = 0
    trial_list = range(n_trials)
    n_threads_list = [32]
    block_size_list = [2000000, 3000000, 4000000]
    time_between_blocks_list = [32.]
    avg_txps = 20000
    n_nodes_list = [512, 1024, 2048, 4096]
    relay_list = [None]
    average_bandwidth_list = [32000000, 64000000]
    average_latency_list = [0.1]
    finality_list = [64]

    config_list = [(offset_trials + trial,
                    threads,
                    block_size,
                    time_between_blocks,
                    avg_txps,
                    n_nodes,
                    relay,
                    average_bandwidth,
                    average_latency,
                    finality,
                    None,
                    None)
                   for trial in trial_list
                   for threads in n_threads_list
                   for block_size in block_size_list
                   for time_between_blocks in time_between_blocks_list
                   for relay in relay_list
                   for average_bandwidth in average_bandwidth_list
                   for average_latency in average_latency_list
                   for finality in finality_list
                   for n_nodes in n_nodes_list]


elif xp == "xp_latency":
    
    n_proc = 3 # NUMBER OF XPS IN PARALLEL
    
    n_trials = 10
    offset_trials = 0
    trial_list = range(n_trials)
    n_threads_list = [32]
    block_size_list = [4000000]
    time_between_blocks_list = [32.]
    avg_txps = 20000
    n_nodes_list = [1024]
    relay_list = [None]
    average_bandwidth_list = [32000000, 64000000]
    average_latency_list = [0.05, 0.075, 0.1, 0.125, 0.15]
    finality_list = [64]

    config_list = [(offset_trials + trial,
                    threads,
                    block_size,
                    time_between_blocks,
                    avg_txps,
                    n_nodes,
                    relay,
                    average_bandwidth,
                    average_latency,
                    finality,
                    None,
                    None)
                   for trial in trial_list
                   for threads in n_threads_list
                   for block_size in block_size_list
                   for time_between_blocks in time_between_blocks_list
                   for n_nodes in n_nodes_list
                   for relay in relay_list
                   for average_bandwidth in average_bandwidth_list
                   for average_latency in average_latency_list
                   for finality in finality_list]

if xp == "xp_attack":
    
    n_proc = 3 # NUMBER OF XPS IN PARALLEL
    
    n_trials = 10
    offset_trials = 0
    trial_list = range(n_trials)
    n_threads_list = [32]
    time_between_blocks_list = [32.]
    avg_txps_list = [2000, 4000]
    def block_size_fun(n_threads, time_between_blocks):
        return int(time_between_blocks*4000000/n_threads)
    n_nodes_list = [1024]
    relay_list = [None]
    average_bandwidth_list = [32000000]
    average_latency_list = [0.1]
    finality_list = [64]
    p_list = [0.01, 0.05, 0.1, 0.2, 0.3]
    s_list = ["all"]

    config_list = [(offset_trials + trial,
                    threads,
                    block_size_fun(threads, time_between_blocks),
                    time_between_blocks,
                    avg_txps,
                    n_nodes,
                    relay,
                    average_bandwidth,
                    average_latency,
                    finality,
                    p,
                    s)
                   for trial in trial_list
                   for threads in n_threads_list
                   for time_between_blocks in time_between_blocks_list
                   for avg_txps in avg_txps_list
                   for n_nodes in n_nodes_list
                   for relay in relay_list
                   for average_bandwidth in average_bandwidth_list
                   for average_latency in average_latency_list
                   for finality in finality_list
                   for p in p_list
                   for s in s_list
                   if threads <= time_between_blocks]

# RESULT CSV
with open(result_filepath, 'w') as csvfile:
    writer = csv.writer(csvfile, delimiter=',')
    writer.writerow(["Trial",
                     "Threads",
                     "BlockSize",
                     "TimeBetweenBlocks",
                     "AverageTxps",
                     "Nodes",
                     "Bandwidth",
                     "Finality",
                     "NumberOfBlocks",
                     "NumberOfStaleBlocks",
                     "AverageThroughput",
                     "AverageConfirmationTime",
                     "StddevConfirmationTime",
                     "SimuTime",
                     "interrupted",
                     ])


def f(args):
    cmd = "python3 run.py " + experiment_name + " " + result_dir + " "
    for arg in args:
        cmd = cmd + str(arg) + " "
    print(cmd)
    p = subprocess.Popen(cmd, shell=True)
    p.wait()


if __name__ == '__main__':

    pool = multiprocessing.Pool(processes=n_proc)
    for _ in tqdm.tqdm(pool.imap_unordered(f, config_list),
                       total=len(config_list)):
        pass
