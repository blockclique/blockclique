#!/usr/bin/env python
import sys
import csv

from blockclique_pos.experiment import Experiment


def run(experiment_name, log_dir, trial, n_threads, block_size,
        time_between_blocks, average_transactions_per_second,
        n_nodes, average_bandwidth, 
        average_latency, finality):

    duration = 3600. * time_between_blocks / n_threads # 3600 blocks

    n_threads = n_threads
    n_nodes = n_nodes
    time_between_blocks = time_between_blocks  # seconds
    transaction_size = 1040  # bits
    block_header_size = 376 + 32 * n_threads  # bits
    max_transactions_per_block = ((block_size - block_header_size)
                                  / transaction_size)
    average_transactions_per_second = average_transactions_per_second
    block_verification_time = 0.050  # second per block
    transaction_verification_time = 0.000025  # second per transaction
    average_bandwidth = average_bandwidth  # bits per second (upload)
    average_latency = average_latency  # second
    finality = finality
    
    xp = Experiment(trial,
                    n_threads,
                    n_nodes,
                    time_between_blocks,
                    max_transactions_per_block,
                    average_transactions_per_second,
                    block_verification_time,
                    transaction_verification_time,
                    transaction_size,
                    block_header_size,
                    average_bandwidth,
                    average_latency,
                    finality,
                    log=False)

    xp.run(duration)
    xp.compute_stats()


    result_dir = "../results/" + experiment_name
    result_filepath = result_dir + "/results.csv"
    with open(result_filepath, 'a') as csvfile:
        writer = csv.writer(csvfile, delimiter=',')
        writer.writerow([trial, n_threads, block_size, time_between_blocks,
                         average_transactions_per_second,
                         n_nodes, average_bandwidth, average_latency,
                         finality,
                         xp.stats["Number of Blocks"],
                         xp.stats["Number of Stale Blocks"],
                         xp.stats["Average Transaction Throughput"],
                         xp.stats["Average Confirmation Time"],
                         xp.stats["Stddev Confirmation Time"],
                         xp.stats["Simulation Time"],
                         xp.stats["interrupted"]
                         ],)

    return xp


if __name__ == "__main__":

    experiment_name = sys.argv[1]
    log_dir = sys.argv[2]
    trial = int(sys.argv[3])
    n_threads = int(sys.argv[4])
    block_size = int(sys.argv[5])
    time_between_blocks = float(sys.argv[6])
    average_transactions_per_second = float(sys.argv[7])
    n_nodes = int(sys.argv[8])
    average_bandwidth = float(sys.argv[9])
    average_latency = float(sys.argv[10])
    finality = int(sys.argv[11])

    run(experiment_name, log_dir, trial, n_threads, block_size,
        time_between_blocks, average_transactions_per_second, 
        n_nodes, average_bandwidth, 
        average_latency, finality)
